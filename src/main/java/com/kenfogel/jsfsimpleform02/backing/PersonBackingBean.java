package com.kenfogel.jsfsimpleform02.backing;

import com.kenfogel.jsfsimpleform02.controller.PersonJpaController;
import com.kenfogel.jsfsimpleform02.entities.Person;
import java.io.Serializable;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Controller class for index.xhtml
 *
 * @author Ken
 */
@Named
@RequestScoped
public class PersonBackingBean implements Serializable {

    private final static Logger LOG = LoggerFactory.getLogger(PersonBackingBean.class);

    @Inject
    private PersonJpaController personJpaController;

    private Person person;

    /**
     * Client created if it does not exist.
     *
     * @return
     */
    public Person getPerson() {
        if (person == null) {
            person = new Person();
        }
        return person;
    }

    /**
     * Save the current person to the db
     *
     * @return
     * @throws Exception
     */
    public String createPerson() throws Exception {
        personJpaController.create(person);
        return null;
    }
}
